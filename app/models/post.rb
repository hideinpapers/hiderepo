class Post < ActiveRecord::Base
    #validacion del texto
    validates :title, presence: true           
    validates :title, length: { in: 5..30 ,    #permite la longitud de valores 
        message: "TITULO INVALIDO!!"}
    
    
    #validacion del contenido
    validates :body, presence: true
    validates :body, length: {minimum:10 ,     #valores minimos permitidos
    message: "El texto de la descripcion es muy corto"}
    validates :body, length: { maximum: 50 ,   #valores maximos permitidos
        message: "El texto de la descripcion es demaciado grande"}
    
    has_many :comments, dependent: :destroy 
    validates_presence_of :title #realiza tablas 
    validates_presence_of :body 


        
      
end


	